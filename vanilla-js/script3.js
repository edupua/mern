// section: FUNCTIONS and ITERATIONS

// chapter: CREATING_FUNCTION
// there are many ways to make a function

// function declaration:
function print_details(name, age) {     // most common way
    console.log(name, age);
    return true;
}
// in the definition-side the variables are called parameters, when you pass in the values you call them arguments

// function expression:
const print_details_v2 = function (name, age) {
    console.log(name, age);
}
console.log(print_details_v2('adff',234));


// chapter: OPTIONAL ARGUMENTS
function print_stuff(name="Chuck", age=23) {
    console.log(name, age);
}
print_stuff();

// you can have variable number of arguments
function variable_argument_function(...my_args) {
    console.log(my_args);
}
variable_argument_function(1,2,3,4,5);
variable_argument_function(1,2,3,true,5,6,7,'aafda');

// scope:
// var is function scope
// let and const are block scoped like other languages {}


// chapter: FUNCTION DECLARATION
// js does auto-hoisting, where all the function and variable name (var) declarations are brought upto top like a header in C but auto

// Arrow functions - similar to closures
const add = (a,b) => {
    return a + b;
}
console.log(add(5,22));


// immediately invoked functions - wierd stuff don't use if you don't know what u are doing
(function (name){
    console.log(name);
})('Atkin')

// explanation: its like a function with no name, (usually not allowed), but with extra () around function you can make it to allow
// second parenthesis is just calling the first

// section: EXECUTION CONTEXT

// learn debugger

// chapter: CONDITIONALS
// syntax same as C, even switch case

// but switch-case is used more widely even with bigger conditionals
const x = 14;
switch(true) { // just use a true and put conditions next
    case x > 13 && x < 52:
        console.log('number is enough');
        break;
    default:
        console.log('number is too low or too much');
}


// falsy values - get coerced to false by js

// bool - false
console.log(Boolean(false));
// number - 0
console.log(Boolean(0));
// string - empty - ""
console.log(Boolean(""));
// other - null, undefined, NaN
console.log(Boolean(null),Boolean(NaN),Boolean(undefined));

// tricky not falsy things
console.log(Boolean([]),Boolean({}),Boolean(function(){}), Boolean(()=>{}) ) ;


// chapter: LOGICAL OPERATORS
// unlike other languages in js logaical operators are used in regular statements also
const array = []
let first_element = array.length > 0 && array[0];
console.log(first_element);
array.push('asdf')
first_element = array.length > 0 && array[0];
console.log(first_element);

// other way to do this is: condition && do_something;
// if condition not satisfied then do_something will never be reached

// otherwise operator ||: if left part fails only then right part executes
// nullish operator ??: if left part is null or undefined only then assign the right one
let c = 10 ?? 20
console.log(c);
c = null ?? 20
console.log(c);

// chapter: LOGICAL ASSIGNMENT
c ||= 30; // assigns only if c was a falsy
console.log(c); // doesn't asign
c = false;
c ||= 30
console.log(c);

// chapter: TERNARY OPERATOR just like C
const age = 20
// syntax: condition ? if true : otherwise
age > 21 ? console.log('You can drink anything') : console.log('You can drink water');


// TERNARY ASSIGNMENTS and MULTIPLE statement TERNARY
let thing;
let title;
const answer = age > 16 
    ? ( title='Boomer', thing = 'games', `${title} you are too old for ${thing}`) 
    : ( title='Zoomer',thing = 'driving', `${title} you are too young for ${thing}`)

console.log(answer);


// section: LOOPS

// for loops, while loops are same as C, along with break and continue, same with while and do while 
// do while loops are often used when something should be executed atleast once.

// idiomatic js loops

// for of loops:

const arr = [1,2,3,['somethign','asdf']];
for ( const item of arr ) { // just like for in python, rust ,, // you don't really need the 'const'
    console.log(item);
}

// for in, in js is used for objects and used to get indices in arrays
const obj = { a:1,b:2,c:[2,3] }

for (prop in obj){
    console.log(prop);
}

// really popular
for (prop in arr){
    console.log(prop, arr[prop]);
}


// chapter: FOREACH 
// like for_each in rust, takes a closure / arrow function / any function
arr.forEach( item => console.log(item*24) )

//  there are some additional arguments that can be passed, which will snap to index, and array
arr.forEach( (item, index, array_itself) => console.log(item, index, array_itself) )


// if function is very big you can just call the function

function my_func (item, index, array) {
    console.log('item is', item);
    console.log('index is', index);
    console.log('array is', array);
}

arr.forEach(my_func);


// function: FILTER
// just like in rust, takes in an argument of iteration (element returned by forEach), 
// and this time instead of a statement there is a condition, 
// the autocollects these into an array which later gets returned
const numbers = [1,2,3,4,5,6,7,8,9];
const multiples_of_3 = numbers.filter( item => item % 3 === 0)
console.log(multiples_of_3);

// function: MAP
// just like rust, takes in an argument of iteration, and performs statement to each item and 
// then reutrns an array output
const numbers_operated = numbers.map( item => item*15 + 3 );
console.log(numbers_operated);

// you need not return an array of the same thing, you can build a completely new array of different things, out of each element
const number_object = numbers_operated.map( number => {
    const ones_digit = number % 10;
    const tens_digit = ( number - ones_digit )/10 % 10;
    return { tens_digit, ones_digit };
})
console.log(number_object);

// you can chain any of these methods, usually filter().map(), is usually chained

// funtion: REDUCE
// used to return a single value by performaing custom operation on array, like sum or product
const sum = numbers.reduce( (acc, item) => acc + item ) // the first argument accumulator stays constant, the second it iterative item
console.log(sum); 

// there is also an optional default value of the accumulator
const sum_2 = numbers.reduce( (acc, item) => acc + item, 23 ) // the first argument accumulator stays constant, the second it iterative item
console.log(sum_2); 


