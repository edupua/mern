// chapter: NUMBERS

// you can use toFixed or toPercision, toExponential to get exact significant figures in each type
console.log((123).toFixed(3));
console.log((123).toPrecision(3));
console.log((123).toExponential(3));

// chapter: MATH related objects

// useful math functions like square root, trignometric functions, etc. 
let x;
console.log(Math.sqrt(9));
console.log(Math.ceil(4.2), Math.floor(4.2));
console.log(Math.pow(7,2)); // use this instead of **
console.log(Math.random()); // gives uniform random number from 0 to 1

// dates
console.log(Date(2021, 0, 10, 12, 30, 0));
console.log(Date('2022-07-10')); // js is intelligent enough to get the time
// usually you initialize a date and get things from ti
const date = new Date();
console.log(date.getFullYear(), date.getSeconds());

// get month though HAS a zero indexer
console.log(date.getMonth(), date);

// depending on the client's location: this library helps fetch auto format
console.log(Intl.DateTimeFormat('default').format(date));


// Section: ARRAYS & OBJECTS

// chapter: basic arrays

const array = [12,123,true,12423,'asdfaf']  // array in js can be a mixture of anything
console.log(array);
const also_array = new Array(12,123,true,12423,'asdfaf') // basically the same as above: no difference
console.log(Array.of(12,123,true,12423,'asdfaf')); // another shitty way to make an array

// get length using length property
console.log(array.length);


// you can set the index at random locations, it generates some empty slots
array[10] = 'somethings'
console.log(array);
array.push('adsf'); // but if you push again, it dissapears into the void for the first time
array.push('adsf'); 
array.push('adsf');  // next time onwards they show up: js is wierd indeed
array.push('adsf'); 
array.push('adsf'); 

// pop works as intended, but here are some new methods
array.unshift(234) // add element at left
console.log(array);
console.log(array.shift()); // remove element at left
console.log(array);

// splice (not slice): used to remove a sub-array
console.log(array.splice(4,6), array); // the 6 is including the 4th index, removed the empty part


// array can be converted into a string (doesn't remove commas or concatenate)
console.log(array.toString());

// chapter: array nesting, concat, spread
// you can just concatenate
let concat = [1,2,3,4,5] + [6,7,8,9,10]; // wrong way of concat (python way)
console.log(concat, typeof concat);
concat = ([1,2,3,4,5]).concat([6,7,8,9,10]); // wrong way of concat (python way)
console.log(concat, typeof concat);

// spread operator, like (*list)  in python
concat = [ ...([1,2,3,4,5]), ...([6,7,8,9,10])  ] 
console.log(concat, typeof concat);
// you don't actually need the brackets
concat = [ ...[1,2,3,4,5], ...[6,7,8,9,10]  ] 
console.log(concat, typeof concat);


// flatten like numpy
console.log( [1,2,3,[4,5,6], [1,[2,3]]].flat()  ); // but only first for 1st iteration, not recursive

// since array is object for 'typeof', we use Array.isArray() function
console.log(Array.isArray([123,24,532,'af']), Array.isArray('asdff'));


// chapter: OBJECTS
const person = {
    name: 'John',
    age: 23,
    hobbies: ['music', 'sport'],
    college: {
        name: 'Alabama State University',
        state: 'Alabama'
    },
    'last name': 'Smith',
}
// left parts are called properties(they can be strings too), right are called values (remember properties don't require brackets() )
console.log(person.college.name);

// but they do require normal brackets in some cases when string is used as property
console.log(person['last name']);

// You can always create new properties on the fly
person.isDev = false  // thus sometimes we define empty objects using const my_object = {} OR new Object()
console.log(person);
// YOu can delete properties
delete person.age;

// defining functions inside properties
person.my_function = function() {
    console.log('Hello World');
}

person.my_function();

// you can spread objects, the properties and values, couple get spread 
const obj1 = {
    a: 1,
    b: 2,
}
const obj2 = {
    c: 3,
    d: 4,
}
const obj3 = { ...obj1, ...obj2}
console.log(obj3);
// you can also use assign function to assign the resulting object the properties of given object
const obj4 = Object.assign(obj1, obj2);
console.log(obj4);

// just like python dictionary .keys exist, .values also work
// just like .items(), you have .entries() which give you a list of 2 values ( tuples don't exist in js)

// to check if property exits
console.log(person.hasOwnProperty('name'));


// for properties with same variable name, just like rust you don't need to write them twice
const state_name = "Alabama"
const established = 1768
const director = "Pr. Michael Radrichard"

const college = {
    state_name,
    established,
    director,
}

// chapter: DESTRUCTURING - pretty tricky

// destructuring arrays is same as that in python

// lets destructure our person object
console.log(person);
const {
    name: person_name,
    hobbies: person_hobbies,
    college: {
        name: college_name,
        state: college_state,
    }
} = person;

const numbers = [23,24,52,24];

console.log(person_name, person_hobbies, college_name, college_state);


// when destructuring a array
const arr = [1,2,3,4,5,6,7]
const [ one, two, ...rest ] = arr
console.log(one, two, rest);


// chapter: JSON ( JavaScript Object Notation )
// array of JSON objects, which are very inter-convertible with other languages also like Java, JavaScript Objects (obviously), python dictionaries

// difference between JSON object and js objects is that keys are always strings, and values can't be functions.

// in js there is very convinient object called JSON object
const person_str = JSON.stringify(person);
console.log(person_str);

// to get back JSON -> JS object
const getback_person = JSON.parse(person_str);
console.log(getback_person);

