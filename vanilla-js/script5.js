// chapter: Replacing DOM elements
// you have the replaceWith function for HTMLElement object (called on the element itself)
const item = document.querySelector('li');
const new_item = document.createElement('li');
new_item.innerText = 'new apples';
item.replaceWith(new_item);

// you can also use outer-html (not like inner) - old way
const all_items = document.querySelectorAll('li');
all_items.forEach(element => {
    const new_element = document.createElement('li');
    new_element.innerText = 'replaced element';
    element.replaceWith(new_element);
});

// you can also call function from parent perspective using replaceChild (called on the parent)
// works just like insertBefore

// chapter: Removing DOM elements
// you have remove (called on the element itself) and removeChild (called on the parent)
const clear = document.querySelector('#clear');
//clear.remove();

const list = document.querySelector('ul');
const second_child = list.children[1];
list.removeChild(second_child);


// section: Events

// chapter: Changing Classes
// first a fall add a basic action to the button (in build method in button elements)
const itemList = document.querySelector('.item-list');
const classList = itemList.classList;
function my_run_function () {
    console.log(classList);
    classList.toggle('dark');
    // the is also classList.replace();

    // you change virtually any style from js
    itemList.style.lineHeight = '3';
}

const button = document.querySelector('.run');
button.onclick = my_run_function;  // value of this parameter is a function


// chapter: Types of listeners
// there are 3 ways to put a listener

// first: in the HTML itself put a onclick attribute and put a function name, 
// which is defined in linked js files
// bad because in-line scripts are bad: <button onclick="run_function()">run</button>   

// second: use onclick function in js like did above with classes

// third: like a sane person use an eventListener (you can add multiple that's the advantage)
clear.addEventListener('click', () => 
    console.log('clear')
);
clear.addEventListener('click', () => 
    console.log('clear second time')
);

// better way, why? tell u later
const onClick = () => console.log('clear the third');
clear.addEventListener('click', onClick);  // you need to unfortunately remember the names of these

// every event can be done automatically by js
clear.click();

// you can also remove the event listener, but need to specify the function too.
clear.removeEventListener('click', () =>  // this doesn't work because this inline function is a new one
    console.log('clear')
);
// use this instead, that's why better way
clear.removeEventListener('click', onClick);


// finally make clear button do its thing what is meant to be
const clearElements = () => {
    const listItems = document.querySelector('.items');
    const children = Array.from(listItems.children);
    children.forEach( element => {
        element.remove();
    }) 
};

clear.addEventListener('click', clearElements);
// 'click' is just an example you can also have stuff like
// 'dblclick', 'contextmenu', 'mousedown' (press any hold), 'mouseup' (release any hold), 
// 'wheel', 'mouseover', 'mouseout', 'dragStart' (when you start dragging), 'dragend' (when you are done dragging)
// 'drag', while you drag (count increases besed on change in speed)

// the event object
// there is always an optional parameter for any eventlistener function called event object
// this contains usefull information about the event
function logoOnClick(e) {
    console.log(e);
    console.log(e.timeStamp);
    // prevent default is a useful 
    e.preventDefault();
    console.log('i prevented the default');
}
const logo = document.querySelector('img');
logo.addEventListener('click', logoOnClick);


// notice all events above were ou

const itemInput = document.querySelector('#item-input');

const onKeyPress = e => {
    // all keyboard events have these options
    console.log(e.key); // informal name for key, keyName
    console.log(e.keyCode); // number from 0 to 60 or something
    console.log(e.code); // standardized names

    // there are also boolean type parameters for modifier keys
    console.log('Shift: ' + e.shiftKey);
    console.log('Control: ' + e.ctrlKey);
    console.log('Alt: ' + e.altKey);
};
const onKeyDown = e => {
    console.log('keydown');
}
const onKeyUp = e => {
    console.log('keyup');
}

itemInput.addEventListener('keypress', onKeyPress);
itemInput.addEventListener('keyup', onKeyUp);
itemInput.addEventListener('keydown', onKeyDown);