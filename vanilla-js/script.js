// sends an altert to the website

// ; are optional, but be consistent
//alert('hello world')

// chapter: CONSOLE

// console object has methods like clear, debug, log, error, debug, table
console.log('This prints out in console, used for debuggig in black');
console.log(100) // logged in green
console.log(true) // logged in green
console.log(100, "black", true)

console.error('red color')
console.warn('yellow color')

// dictionary like two column output
console.table({ "row1": "col2", "row2":"col2 but on row2"})

// you can format console using css
const styles = 'background-color: pink; padding: 10px; color: blue'
console.log("%cMy formatted log", styles)

// chapter: VARIABLES

// const - (not mutable, requires defining), let - (local / defining not mandatory), var - outdated ( unrestricted and global, dont embarass yourself)

// - variables in js require initializing/defining like C but 'var' though just hoists variables, (like puting it in header file of some sort)
// - variables  do not need type specification (dynamic typing)
let name = 234; // auto 

// const is not really mutability, like you can push to an array initialized by 'const'
const arr = [1,2,3,4];
arr.push(5);
console.log(arr);

// const also means you have partial mutability for objects
const info  = {
    name: "guy",
    age: 23,
}

info.name = "notguy"
console.log(info);


// chapter: DATATYPES

// two types of datatypes: primitive (language defined), reference types (objects, custom defined)

// primitive: string, ', ", ` are the same
// primitive: number, no different float and stuff
// primitive: Boolean
// primitive: Null
// primitive: Undefined ( instead of literal garbage in C )
// primitive: Symbol ( Built-in object returns a unique symbol)
// primitive: BigInt ( number type is not mean to handle u_int64 type stuff)

// storage: stored in stack

// reference: Object literals, arrays, functions (methods)
// storage: stored in heap (points to already existing memory, changing one instance will change all)

// since js is dyamic you have a 'typeof' macro
const firstName = "Jamie" 
const age = 14.234  // numbers can have random _ in between like rust, useful for human readability 1_000_000 million
const is_same = true
console.log(firstName, typeof firstName, age, typeof age, is_same, typeof is_same)

// for fun, typeof returns string
console.log(typeof firstName, typeof typeof firstName);

const some_null = null
// null has same type as object
console.log(some_null, typeof some_null);

// you can set variables as undefined even though never used
let forgot_to_define_variable;
const my_undefined_variable = undefined;
console.log( forgot_to_define_variable, typeof forgot_to_define_variable);
console.log( my_undefined_variable, typeof my_undefined_variable);

// use Symbol function to make a symbol
const my_symbol = Symbol('some string');
console.log( my_symbol, typeof my_symbol);

// use 'n' at the end of integer to initialize BigInt
const big_number = 90n; // big integer need not be big
console.log(big_number, typeof big_number);


// both arrays and custom objects are 'objects'
const array = [1,2,3,4];
const my_object = {
    para1: 'adsf',
    para2: 234,
}
console.log(array, typeof array, my_object, typeof my_object);


// functions have their own type called 'function', just to be more specific
function getName() {
    return 'nameof person';
}
console.log(getName, typeof getName);
// to actually call a function, use brackets
console.log(getName());


// chapter: TYPE CONVERSION

// string to number: most used
let number_str = '100';
let number = parseInt(number_str);  // parseFloat(number_str) works same for floats, default does floor
// new way of doing this: unary operator
let also_number = +number_str;
// old way of doing this: Number constructor
let Also_number = Number(number_str);

console.log(number_str, typeof number_str, number, typeof number, also_number, typeof also_number, Also_number, typeof Also_number);

// number to string: second most used
converted_number_str = number.toString();  // ring a bell rustoid
console.log(converted_number_str, typeof converted_number_str);

// number to boolean
const zero = 0;
const non_zero = -142;
const should_be_false = Boolean(zero);
const should_be_true = Boolean(non_zero);
console.log( should_be_false, typeof should_be_false, should_be_true, typeof should_be_true)

// NaN (Not a Number): when you try to force something into a number, basically a number but errored out
const not_a_number = 1 + NaN;
const also_not_a_number = parseInt('some string');  // or like 'some string'/3
const Also_not_a_number = undefined + 23;
const AAlso_not_a_number = Math.sqrt(-1); // complex numbers not supported

console.log( not_a_number, typeof not_a_number, also_not_a_number, typeof also_not_a_number, Also_not_a_number, typeof Also_not_a_number, AAlso_not_a_number, typeof AAlso_not_a_number);


// chapters: OPERATORS

// common arithmetic operators: +, -, *, /, %, **,  ^ (bit-wise XOR)

// concatenate
console.log('first' + 'second');

// C like ++, -- also works
let x = 23;
console.log(x++);
console.log(x);
console.log(++x);
console.log(x);

// comparisions: ==, ===, <, >, ! (not)
// === is specific to js, used to compare types too.
console.log(2 !== '2');  // type along with !

// all prioritasation is same in js as C


// chapter: TYPE COERSION (automatic type casting)

// js likes converting + with concatenate with more priority
// +: NaN > String > Number > Boolean
console.log('string' + 52);
console.log('24' + 52);
console.log(+'24' + 52);

// js likes to convert * with multiplication with more priority
// *: NaN > Number > Boolean > String 
console.log('55'*2);
console.log('string'*2);

// js is just stupid sometimes
let undefined_variable; 
console.log('string' + undefined_variable);


// chapter: STRINGS

// template literals: like format in python
const first_name = 'John'
const salary = 110_000;

x = `Hello, my name is ${first_name}, ${salary}`
console.log(x);

// properties don't need parenthesis because they are not functions, they are variables
console.log(x.length);
console.log(x[0], x[1]);

// to access all methods of an object: use __proto__
console.log(x.__proto__);
// or just create a new object
let s = new String(x)
console.log(s, typeof s)

// most methods for string are same as python stuff
// there is no easy slicing like python [i:e], but we have
console.log(x.substring(2,5));
console.log(x.slice(2,13));
console.log(x.slice(2,-17));

// dereference in objects using valueOf()
console.log((new String('string').valueOf()));
