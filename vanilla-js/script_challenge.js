// captalize first letter
let s = "mystring"
console.log(s[0].toUpperCase() + s.slice(1))

// random numbers and operators
const x = Math.floor(Math.random() * 100 + 1);
const y = Math.floor(Math.random() * 50 + 1);
console.log(`${x} + ${y} = ${x+y}`);
console.log(`${x} - ${y} = ${x-y}`);
console.log(`${x} * ${y} = ${x*y}`);
console.log(`${x} / ${y} = ${Math.floor(x/y)}`);
console.log(`${x} % ${y} = ${x%y}`);

// basic array operations
const arr = [1,2,3,4,5];
arr.unshift(0)
arr.push(6)
console.log(arr.reverse());

// concatenate but remove last element
const arr1 = [1,2,3,4,5]
const arr2 = [5,6,7,8,9,10]
console.log(arr1.concat(arr2.slice(1)));


//  creating a simple object
const library = 
[
    {
        title: "New Testament debunked",
        author: "Chud",
        status: {
            own: true,
            reading: false,
            read: false,
        }
    },
    {
        title: "New Testament debunked - 2",
        author: "Chuck",
        status: {
            own: true,
            reading: false,
            read: false,
        }
    },
    {
        title: "New Testament debunked - 3",
        author: "Norris",
        status: {
            own: true,
            reading: false,
            read: false,
        }
    },
]

// change the flags to true
console.log(library);
library[0].status.read = true; 
library[1].status.read = true; 
library[2].status.read = true; 
console.log(library);

// destructure the book name of first book
const {title: firstBook} = library[0]
console.log(firstBook);

// jsonify the library
const library_str = JSON.stringify(library);
console.log(library_str);

// make a function to turn farenheit to celsius
const getCelsius = f => (f-32)*5/9;
console.log(`The temperature is ${getCelsius(32)} \xB0C`);

// make a minMax function
const minMax = arr => ({
    min: Math.min(...arr),
    max: Math.max(...arr),
})
console.log(minMax([1,2,3,4,5]));


// IIFE that takes in length and width of the rectangle outputs it to the console in message
(function(length, width) {
    console.log(`The area of rectangle with a length of ${length} or a width of ${width} is ${width*length} `);
})(10,1023)


// make a simple calculator using switch case
const calculator = (a,b,s) => {
    switch(s) {
        case '+':
            return a+b;
            break;
        case '-':
            return a-b;
            break
        case '*':
            return a*b;
            break
        case '/':
            return a/b
            break
        default:
            return 'operator not found'
    }
}
console.log(calculator(5,2,'+'));
console.log(calculator(5,2,'-'));
console.log(calculator(5,2,'*'));
console.log(calculator(5,2,'/'));
console.log(calculator(5,2,'&'));
console.log(calculator(5,2,'a'));


// fizzbuzz in js

for (let i = 1; i <= 100; i++) {
    if ( i % 3 === 0 && i % 5 === 0 ) {
        console.log('FizzBuzz');
    } else if (i%3 === 0) {
        console.log('Fizz'); 
    } else if ( i%5 === 0 ) {
        console.log('Buzz');
    } else {
        console.log(i);
    }
}

// filter usage
const people = [
    {
        firstName: 'John',
        lastName: 'Doe',
        email: 'john@gmail.com',
        phone: '911-144-2425',
        age: 30,
    },
    {
        firstName: 'Jane',
        lastName: 'Poe',
        email: 'jane@gmail.com',
        phone: '223-235-5225',
        age: 22,
    },
    {
        firstName: 'Bob',
        lastName: 'Foe',
        email: 'bob@gmail.com',
        phone: '325-252-5614',
        age: 45,
    },
    {
        firstName: 'Sara',
        lastName: 'Soe',
        email: 'sara@gmail.com',
        phone: '442-524-5222',
        age: 21,
    },
]

const youngPeople = people.filter(person => person.age < 25)
    .map(person => ({ name: `${person.firstName} ${person.lastName}`, email: person.email }) )
console.log(youngPeople);


// add all positive numbers of array
const numbers = [2, -23,24,63,52,11,-24,24]
const sum_positive = numbers.filter(num => num > 0).reduce( (acc, num) => acc + num )
console.log(sum_positive, numbers) ;


// captilize first letter
const array = [ 'myname', 'isidk', 'something' ]
const cap_array = array.map( item => {
    return item[0].toUpperCase() + item.slice(1)
})
console.log(cap_array);

// make custom insert after
const firstItem = document.querySelector('h1');
const parent = firstItem.parentElement;
const newItem = document.createElement('h1');

//newItem.textContent = 'new item before'
//parent.insertBefore(newItem, firstItem);
newItem.textContent = 'new item after'

HTMLElement.prototype.insertAfter =  function(element, target) {
    if (target.parentElement !== this) {
        console.error('Node.insertAfter: Child to insert after is not a child of this node');
    }
    target.insertAdjacentElement('afterend', element);
}

parent.insertAfter(newItem, firstItem);
